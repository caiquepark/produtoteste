import { MatFormFieldModule } from '@angular/material/form-field';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { ProdutoListaComponent } from './components/views/produto-lista/produto-lista.component';
import { ToastrModule } from 'ngx-toastr';
import { HeaderComponent } from './components/views/header/header.component';
import { FooterComponent } from './components/views/footer/footer.component';
import { FormularioDialogComponent } from './components/views/formulario-dialog/formulario-dialog.component';
import { FormsModule } from '@angular/forms';






@NgModule({
  declarations: [
    AppComponent,
    ProdutoListaComponent,
    HeaderComponent,
    FooterComponent,
    FormularioDialogComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    ToastrModule.forRoot(),
    
    
    

  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [FormularioDialogComponent]
})
export class AppModule { }
