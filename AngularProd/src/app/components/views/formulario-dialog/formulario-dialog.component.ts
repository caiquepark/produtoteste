import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Produto } from '../../models/Produto';

@Component({
  selector: 'app-formulario-dialog',
  templateUrl: './formulario-dialog.component.html',
  styleUrls: ['./formulario-dialog.component.scss']
})
export class FormularioDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<FormularioDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: Produto,
  ) { }

  ngOnInit(): void {}

  onCancel(): void {
    this.dialogRef.close();
  }
}
